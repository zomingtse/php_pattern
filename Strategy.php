<?php
//STRATEY PATTERN BERFUNGSI MEMANGGIL FUNGSI YANG SAMA DENGAN BEDA CLASS DIKARENAKAN BERBADA EVEN YANG MENYEBABKAN UNTUK BERPINDAH FUNGSI
interface JumlahStrategy {
	public function Jumlah();
}

class JumlahSedikit
{
	private $data;

	public function __construct(Array $data)
	{
		$this->data = $data;
	}

	public function Jumlah() {
		$jumlah = 0;
		for ($i=0; $i < count($this->data) ; $i++) { 
			$jumlah = $jumlah + $this->data[$i];
		}

		return 'Jumlah Sedikit = '.$jumlah;
	}
}

class JumlahBanyak
{
	private $data;

	public function __construct(Array $data)
	{
		$this->data = $data;
	}

	public function Jumlah() {
		$jumlah = 0;
		for ($i=0; $i < count($this->data) ; $i++) { 
			$jumlah = $jumlah + $this->data[$i];
		}
		$jumlah = $jumlah - 10;
		return 'Jumlah Banyak = '.$jumlah;
	}
}

function Jumlah(Array &$data) {
	if (count($data) > 5) {
		$tempData = new JumlahBanyak($data);
	} else {
		$tempData = new JumlahSedikit($data);
	}

	return $tempData->Jumlah();
}

$data = [1,2,3];
echo Jumlah($data);
<?php
/**
Fungsi singleton adalah pemanggilan memori yang hanya satu kali sehingga tidak menumpuk di memori server
**/

class Database
{
	private static $instance;

	public static function getInstance(){
		if(!isset(Database::$instance))
			Database::$instance = new Database;

		return Database::$instance;
	}

	private function __construct()
	{
	  	// Private konstruk
	}

	public function getQuery() {
		return "SELECT * FROM table";
	}
}

// CEK MEMORI UNTUK DATABASE
$db = Database::getInstance();
$db1 = Database::getInstance();
$db2 = Database::getInstance();

// TERLIHAT DARI MANAJEMEN MEMORINYA
var_dump($db);
var_dump($db1);
var_dump($db2);

class DB {

}

// CEK MEMORI UNTUK DATABASE
$db = new DB;
$db1 = new DB;
$db2 = new DB;

// TERLIHAT DARI MANAJEMEN MEMORINYA
var_dump($db);
var_dump($db1);
var_dump($db2);
<?php
/*
PEMANGGILAN SAMA FUNGSI YANG BEDA HASILNYA MAKA DIBUATKAN DECORATOR PATTERN
*/

//COMPONENT
interface Logger {
	public function Log($msg);
}


//CONCRETE COMPONENT
class FileLogger implements Logger {
	public function Log($msg){
		echo "<p>Logging to <b>FILE</b> : $msg </p>";
	}
}

//DECORATOR
abstract class LoggerDecorator implements Logger {
	protected $logger;

	public function __construct(Logger $logger){
		$this->logger = $logger;
	}

	public function Log($msg) {
		$this->logger->Log($msg);
	}
}

//EMAIL LOGGER
class EmailLogger extends LoggerDecorator {
	public function Log($msg) {
		$this->logger->Log($msg);
		echo "<p>Logging to <b>EMAIL</b> : $msg </p>";
	}
}

//TEXT MESSAGER LOGGER
class TextMessageLogger extends LoggerDecorator {
	public function Log($msg) {
		$this->logger->Log($msg);
		echo "<p>Logging to <b>TEXT MESSAGER</b> : $msg </p>";
	}
}

//PRINT LOGGER
class PrintLogger extends LoggerDecorator {
	public function Log($msg) {
		$this->logger->Log($msg);
		echo "<p>Logging to <b>PRINT</b> : $msg </p>";
	}
}

//FAX LOGGER
class FaxLogger extends LoggerDecorator {
	public function Log($msg) {
		$this->logger->Log($msg);
		echo "<p>Logging to <b>FAX</b> : $msg </p>";
	}
}

$log = new FileLogger();
$log = new EmailLogger($log);
$log = new TextMessageLogger($log);
$log = new PrintLogger($log);
$log = new FaxLogger($log);

$log->Log("Simpan Data");
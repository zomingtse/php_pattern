<?php
//BIASASNYA DIPAKAI UNTUK VIEW ATAU PEMBUATAN SEBUAH BENTUK
interface Bentuk {
	public function gambar();
}

class Posisi {
	public function __construct() {
		return 'Ganteng';
	}
} 

class Kotak implements Bentuk
{
	private $posisi;

	public function __construct($pos)
	{
		$this->posisi = $pos;
	}

	public function gambar() {
		echo "Ini gambar kotak";
	}
}

class KotakFactory {
	public function Buat($tipe) {
		if($tipe == 'Kotak'){
			return new Kotak(new Posisi);
		}
	}
}

$factory = new KotakFactory();
$kotak = $factory->Buat('Kotak');
echo $kotak->gambar();
<?php
// BERFUNGSI UNTUK APABILA MEMANGGIL FUNGSI LAIN TETAP MENGKONEKSI KE FUNGSI LAMA ITULAH ADAPTER
class Facebook {
	public function PostToWall($Pesan) {
		echo "Posting pesan...";
	}
}

interface SocialMedia {
	public function Post($Pesan);
}

class FacebookAdapter implements SocialMedia {
	private $facebook;

	public function __construct(Facebook $facebook){
		$this->facebook = $facebook;
	}

	public function Post($Pesan){
		$this->facebook->PostToWall($Pesan);
	}
}

$facebook = new FacebookAdapter(new Facebook);
$facebook->Post("Ini Pesan baru lagi");